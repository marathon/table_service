Table Service
=========

- bundle
- define table structure in config/record.yml
- rake db:drop db:create db:migrate
- padrino s

Create multiple records

POST http://localhost:3000/records?records[][name]=nick&records[][age]=3&records[][name]=nick2&records[][age]=32

Create single record

POST http://localhost:3000/record?record[name]=rob&record[age]=7

Get all records

GET http://localhost:3000/records

Get single record

GET http://localhost:3000/record/:id

Credit
======

myself
