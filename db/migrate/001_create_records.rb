class CreateRecords < ActiveRecord::Migration
  def self.up
    require 'yaml'
    record = YAML.load_file(Padrino.root('config/record.yml'))
    create_table :records do |t|
      record.each do |field|
        field = HashWithIndifferentAccess.new(field)
        t.send(field[:type].to_sym, field[:name].to_sym, field[:options])
      end
      t.timestamps
    end
  end

  def self.down
    drop_table :records
  end
end
