module Record
  class Decorator < Service::Decorator
    type :record

    property :id
    record = YAML.load_file(Padrino.root('config/record.yml'))
    record.each do |field|
      property field['name'].to_sym
    end
  end
end
