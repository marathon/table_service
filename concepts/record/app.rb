require_relative 'decorator'
require_relative 'record'
module Record
  class App < RecordService::App

    get 'records', :provides => :json do
      Decorator.for_collection.represent(Record.all).to_json
    end

    post 'records', :provides => :json do
      if params[:records]
        if params[:records].is_a? Array
          Decorator.represent(Record.creates(params[:records])).to_json
        else
          {error: "Parameter type: 'records' must be an Array; received #{params[:records].class}: #{params[:records.inspect]}"}.to_json
        end
      else
        {error: "Missing parameter: 'records'"}.to_json
      end
    end

    get 'record/:id', :provides => :json do
      Decorator.represent(Record.find(params[:id])).to_json
    end

    post 'record', :provides => :json do
      if params[:record]
        if params[:record].is_a? Hash
          Decorator.represent(Record.create(params[:record])).to_json
        else
          {error: "Parameter type: 'record' must be a Hash; received #{params[:record].class}: #{params[:record.inspect]}"}.to_json
        end
      else
        {error: "Missing parameter: 'record'"}.to_json
      end
    end

    put 'record/:id', :provides => :json  do
      record_id = params[:id]
      record_params = request.env["rack.input"].read
      record_hash = HashWithIndifferentAccess.new(JSON.parse(record_params))
      params = { id: record_id }.merge(record_hash)

      if params["record"]
        record = Record.find(params[:id])
        record.update(params["record"])
        Decorator.represent(record).to_json
      else
        "error"
      end
    end

    delete 'record/:id', :provides => :json do
      if Record.delete(params[:id])
        Decorator.represent(OpenStruct.new({id: params[:id]})).to_json
      else
        "error"
      end
    end

  end
end
