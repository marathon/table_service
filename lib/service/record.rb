module Service
  class Record < ActiveRecord::Base
    def self.creates(params)
      resources = []
      params.each do |hash|
        resources.push(self.create(hash))
      end
      resources
    end
  end
end
